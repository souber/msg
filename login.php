<?php

//设置共有常量
define('APP_PATH',realpath(__DIR__));    //当前应用程序目录

//引入函数库
include_once 'model/user.php';

//判断用户是否已经提交登录表单
	if(isset($_POST['username'])){		//用户已经提交
		//获取表单数据
		$username = $_POST['username'];
		$userpswd = $_POST['userpswd'];
		//实现用户登录
		$status = doUserLogin($userName,$userPswd);
		
		//实现登录后的处理
			if($status){	//登录成功
				//给出登录成功的提示
				$responseMsgs = '用户登录成功';
			}else{		//登录失败
				//给出登录失败的提示
				$responseMsgs = '用户登录失败';
			}
			include_once 'view/login_success.php';
		
	}else{			//用户没有提交登录表单
	//显示登录表单
	include_once 'view/login.php';
		
	}