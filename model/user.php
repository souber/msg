<?php

//开始回话
session_start();

//引入数据库链接数据库
require_once APP_PATH . 'model/db.php';

//用户登录、注销、注册相关操作

/**
 * 实现用户登录操作
 * @param $userName    用户名
 * @param $userPswd	密码
 * return bool   若用户登录成功，返回true，否则返回false 
 */
function doUserLogin($userName,$userPswd){
	//1.查询用户名和密码是否合法（与数据库校验）
	if(isValidUser($userName,$userPswd)){
	//2.校验成功，实现登录
	$_SESSION['logined_user'] = $userName;
	return true;
	}else{
	//3.校验失败，返回false
	return false;
	}

}

/**
 * 实现用户注销操作
 */
function doUserLogout(){
	unset($_SESSION['logined_user']);
	session_destroy();
}

/**
 * 获取当前处理登录状态的用户名
 * @return 若用户处理登录状态，返回用户名，否则返回false
 */
function getloginedUser(){
	if(isset($_SESSION['logined_user'])){
		return $_SESSION['logined_user'];
	}
	return false;
}
