<?php

//设置分页相关的函数库

/**

 * 生成分页码并返回
 * @param $url    分页码超链接URL的基地址（localhost/wamp/php/msg/index.php）
 * @param $currentPage	当前的页码数
 * @param $pageCount	总页码数
 * @renturn String 当前分页码的HTML代码
 */
	function createPage($url,$currentPage,$pageCount){
		//“上一页”和“下一页”
		$lastPage = $currentPage-1;
		$next		= $currentPage+1;
		
		$html = '<ul>';
		//处理“首页”
		$html .= <<<STR
		<li><a href="{$url}?page=1">首页</a></li>
		<li><a href="{$url}?page=$lastPage">上一页</a></li>			
		<li>{$currentPage}</li>		
		<li><a href="{$url}?page=$next">下一页</a></li>
		<li><a href="{$url}?page={$pageCount}">末页</a></li>
STR;

		$html .= '</ul>';
		
		//返回
		return $html;
	}
	
	//echo createPage('#',3,10);