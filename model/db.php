<?php
//数据库相关操作

/**
 * 初始化数据库链接（建立链接）
 * @param array    $dbConfig	数据库链接配置参数（数组形式）
 */
$dbConfig = require_once  APP_PATH . 'config/db.php';
//print_r($dbConfig);

function	initDbConnect(){
	global $dbConfig;
	//建立数据链接
	$link = mysql_connect("{$dbConfig['db_host']}:{$dbConfig['db_port']}",
		$dbConfig['db_user'],$dbConfig['db_pswd']);
		
	//判断链接是否成功
	if(!$link){
		die('数据库链接失败'.mysql_error($link));	
	}
	//选择数据库
	mysql_select_db('$dbConfig['db_name']');
	//设置数据库链接字符串
	mysql_set_charset('$dbConfig['db_charset']');
}

/**
 * 获取msgs表中所有的留言信息
 * @return array	二位数组形式
 */

function getAllMsgs(){
	//链接数据库
	initDbConnect();
	//获取所有留言信息（只考虑msgs表，暂时不考虑rmsgs表）
	$sql = "select * from msgs";
	$r = mysql_query($sql);
	//处理结果
	$results = array();
	while($row = mysql_fetch_assoc($r)){
		$results[] = $row;	
	}
	//返回结果集
	return $results;
}


	/**
	 * 获取留言板数据库中所有记录的个数
	 * return int
	 */
	function getMsgsCount(){
	//链接数据库
	initDbConnect();
	//获取所有留言信息（只考虑msgs表，暂时不考虑rmsgs表）
	$sql = "select * from msgs";
	$r = mysql_query($sql);
	return mysql_num_rows($r);
}


/**
 * 获取指定页的留言记录,所有记录中的指定页的记录
 * @param int	$page	当前是第几页
 * $param	int	$pageSize		每页的记录个数
 * renturn array()	二位数组
 */
function getMsgsByPageNumber($page = 1,$pageSize = 10){
		//构造当前页开始记录的下标
		 $pageBegin = ($page - 1) * $pageSize;
		//链接数据库
		initDbConnect();
		//获取所有留言信息（只考虑msgs表，暂时不考虑rmsgs表）
		$sql = "select * from msgs limit {$pageBegin},{$pageSize}";
		$r = mysql_query($sql);
		//处理结果
		$results = array();
		while($row = mysql_fetch_assoc($r)){
			$results[] = $row;	
	}
	
	//返回结果集
	return $results;
}

/**
 * 通过主键id获取某条相信信息
 * @param $id	主键
 * @return array 关联数组
 */
function getMsgsById($id){
	//链接数据库
	initDbConnect();
	//获取所有留言信息（只考虑msgs表，暂时不考虑rmsgs表）
	$sql = "select * from msgs,users where msgs.m_id = {$id} and msgs.m_u_id = users.u_id";		//关联查询
	$r = mysql_query($sql);
	//处理结果集
	$result = mysql_fetch_assoc($r);
	//返回结果集
	return $result;

}

/**
 * 根据传入的用户名和密码判断用户是否是有效的
 * @param $username
 * @param $userpswd
 * @return boolean 若用户名和密码有效，返回true，否则返回false
 */
function isValidUser($username,$userpswd){
	//链接数据库
	initDbConnect();
	//构造sql语句
	$sql = "select * from users where u_name = '$username' and u_pswd = '$userpswd'";
	//执行sql查询
	$r = mysql_query($sql);
	//处理查询结果
	return mysql_num_rows($r);
}

/**
 * 关关闭数据库链接
 */
function closeConnect(){

	mysql_close();
}