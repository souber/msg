<?php
//显示单条留言的控制器文件

//设置共有常量
define('APP_PATH',realpath(__DIR__));    //当前应用程序目录

//加载数据库
require_once	'model/db.php';
require_once	'model/user.php';

//获取当前登录的用户名
//$logineduser = getloginedUser();

//获取当前要显示的留言的主键id（获取get参数）
$mid = $_GET['m_id'];		//留言id

//开始获取当前主键id所对应的留言信息
$msg = getMsgsById($mid);
//print_r($msg);

//显示视图文件
include_once  APP_PATH . 'view/view.php';

?>