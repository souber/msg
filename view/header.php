<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html    lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>留言板首页</title>
</head>
<body>
	<div	id="header">

<?php 
	if(false == $logineduser){		//用户没有登录
		echo <<<STR
		<a href="login.php">登录</a>
		|
		<a href="register.php">注册</a>
STR;
	}else{
		echo <<<STR
			欢迎您，{'$logineduser'}！
			<a href="logout.php">注销</a>
STR;
	}
?>
</div>