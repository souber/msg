<?php

//设置共有常量
define('APP_PATH',realpath(__DIR__));    //当前应用程序目录

//加载数据库
require_once	'model/db.php';
require_once	'model/user.php';
require_once	'model/page.php';		//分页码相关函数库

//加载分页相关配置文件
$pageConfig = require_once 'config/page.php';
//获取当前页码
	if(isset($_GET[$pageConfig['page_params']])){
		$currentPage =  $_GET[$pageConfig['page_params']];
	}else{
		$currentPage = 1;
	}

		//当前页码健壮性考虑
		//当前页码至少要大于0
		if($currentPage <=0){
			$currentPage = 1;
		}	
	
		//当前页码不能大于总页码
		$pageCount = ceil(getMsgsCount() / $pageConfig['page_size']);
			if($currentPage > $pageCount){
				$currentPage = $pageCount;
			}


//获取当前登录的用户名
$logineduser = getloginedUser();

//获取所有留言
$msgs = getMsgsByPageNumber($currentPage,$pageConfig['page_size']);
//print_r($msgs);

//显示视图文件
include_once 'view/index.php';